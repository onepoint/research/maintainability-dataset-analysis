JAVANALYSER=javanalyser-1.0.0.jar
DATASET=../../maintainability-dataset

function rename_output() {
  mv metrics.csv metrics/metrics_$1.csv
  mv schema.csv schema/schema_$1.csv
  mv broken_references.csv broken_references/broken_references_$1.csv
}

java -jar $JAVANALYSER --path "$DATASET/aoi/sourcefiles" \
  --java-version 8 \
  --classpath "$DATASET/aoi/sourcefiles/lib/bsh-2.0b4.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/Buoy.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/Buoyx.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/gluegen-rt.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/groovy-all-2.1.2.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/Jama-1.0.2.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/jogl.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/QuickTimeWriter.jar" \
  --classpath "$DATASET/aoi/sourcefiles/lib/svgSalamander.jar" \
  --classpath "$DATASET/aoi/sourcefiles/HelpPlugin/lib/helpgui-1.1b.jar" \
  --classpath "$DATASET/aoi/sourcefiles/HelpPlugin/lib/jhall.jar" \
  --classpath "$DATASET/aoi/sourcefiles/HelpPlugin/lib/jhelpaction.jar" \
  --classpath "$DATASET/aoi/sourcefiles/HelpPlugin/lib/pircbot.jar" \
  --classpath "$DATASET/_jars/junit-4.12.jar"
rename_output aoi

java -jar $JAVANALYSER --path "$DATASET/junit4/sourcefiles" \
  --java-version 8 \
  --classpath "$DATASET/junit4/sourcefiles/pitest/lib/pitest-0.32.jar" \
  --classpath "$DATASET/junit4/sourcefiles/pitest/lib/pitest-ant-0.32.jar" \
  --classpath "$DATASET/junit4/sourcefiles/pitest/lib/pitest-command-line-0.32.jar" \
  --classpath "$DATASET/junit4/sourcefiles/lib/hamcrest-core-1.3.jar"
rename_output junit4

java -jar $JAVANALYSER --path "$DATASET/argoUML/sourcefiles" \
  --java-version 8 \
  --classpath "$DATASET/argoUML/sourcefiles/modules/jscheme/lib/jscheme.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/antlr-2.7.7.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/batik-awt-util-1.7.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/batik-dom-1.7.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/batik-ext-1.7.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/batik-svggen-1.7.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/batik-util-1.7.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/batik-xml-1.7.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/commons-logging-1.0.2.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/gef-0.13.8.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/ocl-argo-1.1.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-dom.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-dom4j.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-jdom.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-s9api.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-sql.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-unpack.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-xom.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-xpath.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9-xqj.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/saxon9.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/swidgets-0.1.4.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/lib/toolbar-1.4.1-20071227.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-app/org.eclipse.uml2.uml.resources_3.1.1.v201008191505.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-infra/lib/log4j-1.2.6.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.common_2.6.0.v20100914-1218.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.ecore_2.6.1.v20100914-1218.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.ecore.change_2.5.1.v20100907-1643.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.ecore.change.edit_2.5.0.v20100521-1846.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.ecore.edit_2.6.0.v20100914-1218.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.ecore.xmi_2.5.0.v20100521-1846.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.edit_2.6.0.v20100914-1218.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.mapping_2.6.0.v20100914-1218.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.mapping.ecore_2.6.0.v20100914-1218.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/emf/org.eclipse.emf.mapping.ecore2xml_2.5.0.v20100521-1847.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/uml2/org.eclipse.uml2.common_1.5.0.v201005031530.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/uml2/org.eclipse.uml2.common.edit_1.5.0.v201005031530.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/uml2/org.eclipse.uml2.uml_3.1.1.v201008191505.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/uml2/org.eclipse.uml2.uml.edit_3.1.0.v201005031530.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-euml/lib/uml2/org.eclipse.uml2.uml.resources_3.1.1.v201008191505.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/lib/jmi.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/lib/jmiutils.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/lib/mdrapi.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/lib/mof.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/lib/nbmdr.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/lib/openide-util.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/tools/lib/mdrant-patched.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/src/argouml-core-model-mdr/tools/mdrsrc20050711.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/etc/ant-bootstrap.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-antlr.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-apache-bcel.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-apache-bsf.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-apache-log4j.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-apache-oro.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-apache-regexp.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-apache-resolver.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-commons-logging.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-commons-net.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-jai.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-javamail.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-jdepend.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-jmf.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-jsch.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-junit.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-launcher.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-netrexx.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-nodeps.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-starteam.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-stylebook.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-swing.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-testutil.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-trax.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant-weblogic.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/ant.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/xercesImpl.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/apache-ant-1.7.0/lib/xml-apis.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/antlr.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/checkstyle-4.3.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/checkstyle-all-4.3.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/checkstyle-optional-4.3.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/commons-beanutils-core.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/commons-cli.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/commons-collections.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/checkstyle-4.3/commons-logging.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/classycle/classycle.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/cobertura-1.9/cobertura.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/cobertura-1.9/lib/asm-2.2.1.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/cobertura-1.9/lib/asm-tree-2.2.1.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/cobertura-1.9/lib/jakarta-oro-2.0.8.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/cobertura-1.9/lib/log4j-1.2.9.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/fop-0.20.5/build/fop.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/fop-0.20.5/lib/avalon-framework-cvs-20020806.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/fop-0.20.5/lib/batik.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/fop-0.20.5/lib/xalan-2.4.1.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/fop-0.20.5/lib/xercesImpl-2.2.1.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/fop-0.20.5/lib/xml-apis.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/jdepend-2.9/lib/jdepend-2.9.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/junit-3.8.2/junit.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/lib/easymock12.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/lib/javasrc-2001-beta.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/lib/saxon-6.5.2.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/pmd-4.0/lib/asm-3.0.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/pmd-4.0/lib/jaxen-1.1.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/tools/pmd-4.0/lib/pmd-4.0.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/www/gsoc2007/profiles/proposal0/profile-javabeans.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/lib/java-interfaces.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/pitest/lib/pitest-0.32.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/pitest/lib/pitest-ant-0.32.jar" \
  --classpath "$DATASET/argoUML/sourcefiles/pitest/lib/pitest-command-line-0.32.jar" \
  --classpath "$DATASET/_jars/junit-4.12.jar"
rename_output argouml

java -jar $JAVANALYSER --path "$DATASET/diarymanagement/sourcefiles/tbaclient" \
  --java-version 8 \
  --classpath "$DATASET/diarymanagement/sourcefiles/dbserver/lib/junit/junit-3.8.2.jar" \
  --classpath "$DATASET/diarymanagement/sourcefiles/tbaclient/libs/mousegestures-1.2.jar" \
  --classpath "$DATASET/diarymanagement/sourcefiles/tbaclient/libs/sqlitejdbc-v056-pure.jar"
rename_output diarymanagement-1

java -jar $JAVANALYSER --path "$DATASET/diarymanagement/sourcefiles/dbserver" \
  --java-version 8 \
  --classpath "$DATASET/diarymanagement/sourcefiles/dbserver/lib/junit/junit-3.8.2.jar" \
  --classpath "$DATASET/diarymanagement/sourcefiles/dbserver/lib/CopyLibs/org-netbeans-modules-java-j2seproject-copylibstask.jar" \
  --classpath "$DATASET/diarymanagement/sourcefiles/dbserver/lib/sqlitejdbc-v056-pure.jar"
rename_output diarymanagement-2

java -jar $JAVANALYSER --path "$DATASET/diarymanagement/sourcefiles/tbaclientr/SSL" \
  --java-version 8
rename_output diarymanagement-3

java -jar $JAVANALYSER --path "$DATASET/jsweet/sourcefiles/core-lib/es5" \
  --java-version 8
rename_output jsweet-1

java -jar $JAVANALYSER --path "$DATASET/jsweet/sourcefiles/core-lib/es6" \
  --java-version 8
rename_output jsweet-2

java -jar $JAVANALYSER --path "$DATASET/jsweet/sourcefiles/candy-generator" \
  --java-version 8 \
  --classpath "$DATASET/_jars/commons-io-2.4.jar" \
  --classpath "$DATASET/_jars/commons-lang3-3.3.2.jar" \
  --classpath "$DATASET/_jars/file-visitor-1.0.0.jar" \
  --classpath "$DATASET/_jars/gson-2.2.4.jar" \
  --classpath "$DATASET/_jars/httpclient-4.5.2.jar" \
  --classpath "$DATASET/_jars/httpcore-4.4.5.jar" \
  --classpath "$DATASET/_jars/java-cup-10k.jar" \
  --classpath "$DATASET/_jars/jflex-1.3.5.jar" \
  --classpath "$DATASET/_jars/jsap-2.1.jar" \
  --classpath "$DATASET/_jars/jsoup-1.7.2.jar" \
  --classpath "$DATASET/_jars/jsweet-core-5-20180609.172333-20.jar" \
  --classpath "$DATASET/_jars/junit-4.12.jar" \
  --classpath "$DATASET/_jars/log4j-1.2.17.jar"
rename_output jsweet-3

java -jar $JAVANALYSER --path "$DATASET/jsweet/sourcefiles/transpiler" \
  --java-version 8 \
  --classpath "$DATASET/_jars/commons-lang3-3.7.jar" \
  --classpath "$DATASET/_jars/commons-text-1.3.jar" \
  --classpath "$DATASET/_jars/commons-io-2.4.jar" \
  --classpath "$DATASET/_jars/junit-4.12.jar" \
  --classpath "$DATASET/_jars/log4j-1.2.17.jar" \
  --classpath "$DATASET/_jars/gson-2.2.4.jar" \
  --classpath "$DATASET/_jars/jsap-2.1.jar" \
  --classpath "$DATASET/_jars/ant-1.6.5.jar" \
  --classpath "$DATASET/_jars/closure-compiler-v20161201.jar" \
  --classpath "$DATASET/_jars/ts.core-1.4.0-20181007.101500-10.jar" \
  --classpath "$DATASET/_jars/tools-8.jar" \
  --classpath "$DATASET/_jars/jsweet-core-6.0.1-20190609.211417-5.jar" \
  --classpath "$DATASET/_jars/jquery-1.10.0-20170726.jar" \
  --classpath "$DATASET/_jars/jqueryui-1.11.0-20170726.jar" \
  --classpath "$DATASET/_jars/angular-1.4.0-20170726.jar" \
  --classpath "$DATASET/_jars/angular-route-1.2.0-20170726.jar" \
  --classpath "$DATASET/_jars/node-7.5.0-20170726.jar" \
  --classpath "$DATASET/_jars/express-4.14.0-20170726.jar" \
  --classpath "$DATASET/_jars/express-errorhandler-4.14.0-20170726.jar" \
  --classpath "$DATASET/_jars/express-body-parser-4.14.0-20170726.jar" \
  --classpath "$DATASET/_jars/socket.io-1.4.5-20170726.jar" \
  --classpath "$DATASET/_jars/threejs-75-20171101.jar" \
  --classpath "$DATASET/_jars/javax.ws.rs-api-2.1.jar"
rename_output jsweet-4

# make javanalyser able to choose java version and add to jsweet4 with java8 (after jqueryui):
#  --classpath "$DATASET/_jars/backbone-1.3.0-20170726.jar" \
