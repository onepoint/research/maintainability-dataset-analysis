# README

## Setup development environment

Jupyter notebook and Virtualenv are needed (you may add a `--user` flag if you're not administrator).
```shell
pip install notebook
pip install virtualenv
```

Then create a virtual environment.
```shell
virtualenv --python python3.10 venv
```

Activate your environment, on POSIX:
```shell
source ./venv/bin/activate
```

On Windows:
```shell
.\venv\Scripts\activate
```

Finally, install your virtual environment.
```shell
pip install -r requirements.txt
ipython kernel install --user --name maintainability-dataset-analysis
```

The Jupyter kernel links Jupyter to the installed virtual environment.
To manipulate the Jupyter kernel, these commands are useful:
```shell
jupyter kernelspec list
jupyter kernelspec uninstall maintainability-dataset-analysis
```

You can now launch the Jupyter notebook and select the kernel associated with the virtual environment (ie `maintainability-dataset-analysis`).
```shell
jupyter notebook
```

You can use Jupyter on a server.
You'll need to create the adequate SSH tunnel,
and launch Jupyter as a nohup server.
For simplicity, we use everywhere the same port.
```shell
ssh -L 8081:localhost:8081 user@hostname
nohup python3 -m notebook --no-browser --port=8081 &
```

## Modification to dataset

This is a summary, the [full diff files](./dataset/diff) are in the dataset folder.

- Art Of Illusion:
  - Qualify fully the name for ``artofillusion.procedural.Module``;
  - Replace ``sun.misc.BASE64Encoder`` `with ``java.util.Base64.Encoder``;
  - Replace ``sun.misc.BASE64Decoder`` `with ``java.util.Base64.Decoder``;
- ArgoUML:
  - Delete duplicated ``InitializeModel``;
  - Delete duplicated ``MockModelImplementation``;
  - Fix a parsing problem ([Spoon bug on the scope in switch blocks](https://github.com/INRIA/spoon/issues/4696));
- Diary Management: _No modification_;
- JSweet: _No modification_;
- Junit4: _No modification_.

## Licence

This project and all its content except the software maintainability dataset is released under the MIT License.


The software maintainability dataset is available under the CC-BY-4.0 License.
It was partly included in the folder [dataset/maintainability_experts_labels](./dataset/maintainability_experts_labels).
Here are the associated bibliographical references:
- Schnappinger, M., Fietzke, A., & Pretschner, A. (2020). A Software Maintainability Dataset.
  https://doi.org/10.6084/M9.FIGSHARE.12801215.V3
- Schnappinger, M., Fietzke, A., & Pretschner, A. (2020).
  Defining a Software Maintainability Dataset: Collecting, Aggregating and Analysing Expert Evaluations of Software Maintainability.
  2020 IEEE International Conference on Software Maintenance and Evolution (ICSME), 278–289.
  https://doi.org/10.1109/ICSME46990.2020.00035

